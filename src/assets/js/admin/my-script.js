// function support multi language for datatable Header Tiltle
export function setHeaderTitleSupportMultiLanguge(element, data) {
    $(element).each(function (item, index) {
        var key = $(this).data('key');
        var ln= localStorage.getItem('language');
        var text = data[ln][key];
        $(this).text(text)
    })
}