import {Component, OnInit} from '@angular/core';

declare var $: any

@Component({
    selector: 'app-frontend',
    templateUrl: './frontend.component.html',
})
export class FrontendComponent implements OnInit {

    constructor() {
        $('body').removeClass('bg-theme1 bg-theme');
    }

    ngOnInit() {
    }

}
