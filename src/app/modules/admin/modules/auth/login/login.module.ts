import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {LoginRoutingModule} from './login-routing.module';
import {IbamLoginComponent} from '@shared/components';
import {FormsModule} from '@angular/forms';

@NgModule({
    declarations: [IbamLoginComponent,
    ],
    imports: [
        CommonModule,
        FormsModule,
        LoginRoutingModule
    ]
})
export class LoginModule {
}
