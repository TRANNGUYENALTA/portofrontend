import {Component, Input, OnChanges, OnInit} from '@angular/core';
import {toggleMenu} from './sidebar.js';
import * as locals from './locals';

@Component({
  selector: 'app-sidebar-menu',
  templateUrl: './sidebar-menu.component.html',
  styleUrls: ['./sidebar-menu.component.css']
})
export class SidebarMenuComponent implements OnInit, OnChanges {
  @Input('language') language;
  public _locals: any = locals;

  constructor() {
  }

  ngOnInit() {
    toggleMenu();
  }

  ngOnChanges() {
    const currentLanguage = localStorage.getItem('language');
    if (currentLanguage) {
      this.language = currentLanguage;
    }
  }

}
