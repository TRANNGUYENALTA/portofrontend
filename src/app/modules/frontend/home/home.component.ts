import {Component, OnInit} from '@angular/core';
import {slider} from './home';

declare var $: any

@Component({
    selector: 'app-home',
    templateUrl: './home.component.html',
})
export class HomeComponent implements OnInit {

    constructor() {
        $('header').addClass('header-transparent');
    }

    ngOnInit() {
        slider($);
    }

}
