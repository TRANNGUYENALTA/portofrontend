function onFileChanges(input, imageEl, nameEl) {
    if (input.files && input.files[0]) {
        var reader = new FileReader()
        reader.onload = function (e) {
            if (nameEl) {
                var text = input.files[0].name;
                var type = input.files[0].type
                if (text.length > 12)
                    text = text.slice(0, 12) + '...' + type.slice('6')
                $(nameEl).text(text)
            }
            if (imageEl) {
                console.log(imageEl)
                $(imageEl).attr('src', e.target.result);
            }
        };
        reader.readAsDataURL(input.files[0]);
    }
}
