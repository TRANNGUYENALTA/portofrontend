import {ModuleWithProviders, NgModule} from '@angular/core';
import {RouterModule} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
// import pipes
import {
    SafeHtmlPipe,
    TimingPipe,
    MaxLengthPipe
} from './pipes';

// import component

const BASE_MODULES = [
    HttpClientModule,
    RouterModule,
];
const PIPES = [
    TimingPipe,
    SafeHtmlPipe,
    MaxLengthPipe
];
const DIRECTIVES = [];
const COMPONENTS = [

];

@NgModule({
    imports: [
        ...BASE_MODULES
    ],
    exports: [...PIPES, ...BASE_MODULES, ...DIRECTIVES, ...COMPONENTS],
    declarations: [...PIPES, ...DIRECTIVES, ...COMPONENTS, ]
})
export class SharedModule {
    static forRoot(): ModuleWithProviders {
        return {
            ngModule: SharedModule,
            providers: [],
        };
    }
}
