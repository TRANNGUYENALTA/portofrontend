export const slider = (e) => {
    var o = {
        loop: !0,
        margin: 0,
        responsiveClass: !0,
        nav: !1,
        navText: ['<i class="icon-angle-left">', '<i class="icon-angle-right">'],
        dots: !0,
        autoplay: !0,
        autoplayTimeout: 15e3,
        items: 1
    }, t = e(".home-slider");
    t.owlCarousel(e.extend(!0, {}, o, {
        lazyLoad: !0,
        nav: !0,
        dots: !1,
        autoplayTimeout: 12e3,
        animateOut: "fadeOut"
    })), t.on("loaded.owl.lazy", function (o) {
        e(o.element).closest(".home-slide").addClass("loaded")
    }), e(".featured-products").owlCarousel(e.extend(!0, {}, o, {
        loop: !1,
        margin: 30,
        autoplay: !1,
        navText: ['<i class="icon-left-open-big">', '<i class="icon-right-open-big">'],
        responsive: {0: {items: 2}, 700: {items: 3, margin: 15}, 1200: {items: 4}}
    })), e(".widget-featured-products").owlCarousel(e.extend(!0, {}, o, {
        lazyLoad: !0,
        nav: !0,
        dots: !1,
        autoHeight: !0
    }));
    var n = e(".about-slider");
    n.each(function () {
        e(this).owlCarousel(e.extend(!0, {}, o, {margin: 2, lazyLoad: !0}))
    }), n.on("loaded.owl.lazy", function (o) {
        e(o.element).closest("div").addClass("loaded")
    }), e(".entry-slider").each(function () {
        e(this).owlCarousel(e.extend(!0, {}, o, {margin: 2, lazyLoad: !0}))
    }), e(".related-posts-carousel").owlCarousel(e.extend(!0, {}, o, {
        loop: !1,
        margin: 30,
        autoplay: !1,
        responsive: {0: {items: 1}, 480: {items: 2}, 1200: {items: 3}}
    })), e(".boxed-slider").owlCarousel(e.extend(!0, {}, o, {
        lazyLoad: !0,
        autoplayTimeout: 2e4,
        animateOut: "fadeOut"
    })), e(".boxed-slider").on("loaded.owl.lazy", function (o) {
        e(o.element).closest(".category-slide").addClass("loaded")
    }), e(".product-single-default .product-single-carousel").owlCarousel(e.extend(!0, {}, o, {
        nav: !0,
        dotsContainer: "#carousel-custom-dots",
        autoplay: !1,
        onInitialized: function () {
            var o = this.$element;
            e.fn.elevateZoom && o.find("img").each(function () {
                var o = e(this), t = {
                    responsive: !0,
                    zoomWindowFadeIn: 350,
                    zoomWindowFadeOut: 200,
                    borderSize: 0,
                    zoomContainer: o.parent(),
                    zoomType: "inner",
                    cursor: "grab"
                };
                o.elevateZoom(t)
            })
        }
    })), e(".product-single-extended .product-single-carousel").owlCarousel(e.extend(!0, {}, o, {
        dots: !1,
        autoplay: !1,
        responsive: {0: {items: 1}, 480: {items: 2}, 1200: {items: 3}}
    })), e("#carousel-custom-dots .owl-dot").click(function () {
        e(".product-single-carousel").trigger("to.owl.carousel", [e(this).index(), 300])
    })

}

