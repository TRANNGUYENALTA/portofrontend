import {NgModule} from '@angular/core';

import {Routes, RouterModule, ExtraOptions} from '@angular/router';
import {AppPreloadingStrategy} from './custom-preloading';

const routes: Routes = [
    {path: '', loadChildren: './modules/frontend/frontend.module#FrontendModule'},
    {path: 'admin', loadChildren: './modules/admin/admin.module#AdminModule'},
    {path: '**', redirectTo: '', pathMatch: 'full'}
];

const config: ExtraOptions = {
    useHash: true,
    preloadingStrategy: AppPreloadingStrategy
};

@NgModule({
    imports: [RouterModule.forRoot(routes, config)],
    exports: [RouterModule],
    providers: [AppPreloadingStrategy] // support preloading in angular
})
export class AppRoutingModule {
}
