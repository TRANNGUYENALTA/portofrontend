import {Component, OnInit} from '@angular/core';
import {mobileMenutoggler} from './product';

declare var jQuery: any

@Component({
    selector: 'app-product',
    templateUrl: './product.component.html',
    styleUrls: ['./product.component.css']
})
export class ProductComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
        mobileMenutoggler(jQuery);
    }

}
