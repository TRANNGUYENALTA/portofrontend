export const data = {
  phongban: {
    'uk': {
      name: 'Systems',
      sub_menu: [
        {name: 'System', route: 'systems/system'},
        {name: 'System Type', route: 'systems/system-type'},
        {name: 'IP Whitelist', route: 'systems/ip-whitelist'},
      ]
    },
    'vi': {
      name: 'Phòng Ban',
      sub_menu: [
        {name: 'Chi Tiết PB', route: 'phong-ban/chi-tiet'},
        {name: 'Thên Nhân Viên PB', route: 'phong-ban/them-nhan-vien'},
      ]
    }
  },
  user: {
    'uk': {
      name: 'User',
      route: 'user'
    },
    'vi': {
      name: 'Người Dùng',
      route: 'user'
    }
  },
  log: {
    'uk': {
      name: 'Log',
      route: 'log'
    },
    'vi': {
      name: 'Nhật Ký Log',
      route: 'log'
    }
  },


};
