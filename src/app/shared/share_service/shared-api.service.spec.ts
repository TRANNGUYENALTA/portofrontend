import { TestBed, inject } from '@angular/core/testing';

import { SharedApiService } from './shared-api.service';

describe('SharedApiService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SharedApiService]
    });
  });

  it('should be created', inject([SharedApiService], (service: SharedApiService) => {
    expect(service).toBeTruthy();
  }));
});
