import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { FrontendRoutingModule } from './frontend-routing.module';
import { FrontendComponent } from './frontend.component';
import {HeaderComponent} from './components/header/header.component';
import { MobileMenuOverlayComponent } from './components/mobile-menu-overlay/mobile-menu-overlay.component';
import { MobileMenuContainerComponent } from './components/mobile-menu-container/mobile-menu-container.component';
import { NewsletterComponent } from './components/newsletter/newsletter.component';

@NgModule({
  declarations: [FrontendComponent, HeaderComponent, MobileMenuOverlayComponent, MobileMenuContainerComponent, NewsletterComponent],
  imports: [
    CommonModule,
    FrontendRoutingModule
  ]
})
export class FrontendModule { }
