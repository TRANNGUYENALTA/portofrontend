export const swal_local = {
    'uk': {
        success: {
            title: 'Congratulation!',
        },
        error: {
            title: 'Something went wrong!',
            err_unauthorized: 'Please Sign In Again!'
        },
        confirm: {
            title: 'Are You Sure!',
            message: 'Once deleted, you will not be able to recover!'
        },
        expire_confirm: {
            title: 'Are You Sure!',
            message: 'Once Expire, you will not be able to recover!'
        },
    },
    'vi': {
        success: {
            title: 'Thành Công!',
        },
        error: {
            title: 'Đã Xảy Ra Lỗi!',
            err_unauthorized: 'Vui Lòng Đăng Nhập Lại!'
        },
        confirm: {
            title: 'Bạn Có Chắc Chắn Muốn Xóa!',
            message: 'Sau khi xóa, bạn sẽ không thể phục hồi!'
        },
        expire_confirm: {
            title: 'Bạn Có Chắc Chắn!',
            message: 'Sau khi hết hiệu lực, bạn sẽ không thể phục hồi!'
        },
    }
};