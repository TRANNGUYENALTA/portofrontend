import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {IbamLoginComponent} from '@shared/components';

const routes: Routes = [
    {path: '', component: IbamLoginComponent}
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class LoginRoutingModule {
}
