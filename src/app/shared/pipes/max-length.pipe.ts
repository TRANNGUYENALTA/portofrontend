import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'maxLength'
})
export class MaxLengthPipe implements PipeTransform {

  transform(input: string, args: number): string {
    if (input && input.length > args) {
      return input.slice(0, args);
    } else {
      return input;
    }
  }

}
