import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {PagesComponent} from './pages.component';

const routes: Routes = [
    {
        path: '', component: PagesComponent, children: [
            {path: 'catagory', loadChildren: './catagory/catagory.module#CatagoryModule'},
            {path: 'product', loadChildren: './product/product.module#ProductModule'},
            {path: '', redirectTo: 'catagory'}
        ]
    },
    {path: '**', redirectTo: ''}

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class PagesRoutingModule {
}
