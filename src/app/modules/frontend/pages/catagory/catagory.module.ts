import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { CatagoryRoutingModule } from './catagory-routing.module';
import { CatagoryComponent } from './catagory/catagory.component';

@NgModule({
  declarations: [CatagoryComponent],
  imports: [
    CommonModule,
    CatagoryRoutingModule
  ]
})
export class CatagoryModule { }
