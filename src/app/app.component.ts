import {Component} from '@angular/core';

@Component({
    selector: 'app-root',
    template: '<router-outlet></router-outlet>',
    styleUrls: ['./app.component.css']
})
export class AppComponent {
    title = 'newProject';

    constructor() {
        if (!localStorage.getItem('language')) {
            localStorage.setItem('language', 'uk');
        }

    }
}
