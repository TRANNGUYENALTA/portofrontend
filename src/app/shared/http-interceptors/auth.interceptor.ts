import {Injectable} from '@angular/core';
import {
    HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpHeaders
} from '@angular/common/http';

@Injectable()
export class AuthInterceptor implements HttpInterceptor {

    constructor() {
    }

    intercept(req: HttpRequest<any>, next: HttpHandler) {
        // Get the auth token from the service.
        const authToken = 'Bearer ' + localStorage.getItem('currentUser');
        // Clone the request and replace the original headers with
        // cloned headers, updated with the authorization.
        let headers: HttpHeaders = new HttpHeaders();
        headers = headers.append('Authorization', authToken);
        headers = headers.append('Accept-Language', localStorage.getItem('language'));
        const authReq = req.clone({
            headers: headers,
        });

        // send cloned request with header to the next handler.
        return next.handle(authReq);
    }
}

// req.headers.set('Authorization', authToken)