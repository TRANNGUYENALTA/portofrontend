import {NgModule} from '@angular/core';
import {Routes, RouterModule} from '@angular/router';
import {FrontendComponent} from './frontend.component';

const routes: Routes = [
    {
        path: '', component: FrontendComponent, children: [
            {path: 'pages', loadChildren: './pages/pages.module#PagesModule'},
            {path: 'home', loadChildren: './home/home.module#HomeModule'},
            {path: '', redirectTo: 'home'}
        ],
    },
    {path: '**', redirectTo: 'gate'}

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class FrontendRoutingModule {
}
