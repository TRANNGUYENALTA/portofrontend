import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { IbamLoginComponent } from './ibam-login.component';

describe('IbamLoginComponent', () => {
  let component: IbamLoginComponent;
  let fixture: ComponentFixture<IbamLoginComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ IbamLoginComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(IbamLoginComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
