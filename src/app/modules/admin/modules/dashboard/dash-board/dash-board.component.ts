import {Component, OnInit} from '@angular/core';

declare var $: any

@Component({
    selector: 'app-dash-board',
    templateUrl: './dash-board.component.html',
    styleUrls: ['./dash-board.component.css']
})
export class DashBoardComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
        $('body').addClass('bg-theme1 bg-theme');
    }

}
