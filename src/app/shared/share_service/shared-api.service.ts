import {Injectable, OnInit} from '@angular/core';
import {HttpClient, HttpErrorResponse, HttpHeaders} from '@angular/common/http';
import {ActivatedRoute, Router, RouterState, RouterStateSnapshot} from '@angular/router';
import {Observable, throwError} from 'rxjs';
import {BaseUrlService} from '../base_url/base-url.service';
import {catchError, map} from 'rxjs/operators';
import swal from 'sweetalert';
import * as swal_languages from '@shared/locals/swal_local';

@Injectable({
    providedIn: 'root'
})
export class SharedApiService {
    DEFAULT_ERROR_TITLE = 'Something went wrong';
    public ln = 'uk';

    constructor(private http: HttpClient,
                private baseUrlService: BaseUrlService,
                private router: Router,
                private route: ActivatedRoute
    ) {
    }

    // Get method
    getApi(url: string): Observable<any> {
        const _url = `${this.baseUrlService.getBaseUrl()}` + url;
        return this.http.get<any>(_url)
            .pipe(map((response) => {
                    return response;
                }),
                catchError((error: HttpErrorResponse) => {
                    return this.handleErrorResponse(error);
                })
            );
    }

    // Post method
    postApi(url: string, data): Observable<any> {
        return this.http.post<any>(`${this.baseUrlService.getBaseUrl()}` + url, data)
            .pipe(map((response) => {
                return response;
            }), catchError((error: HttpErrorResponse) => {
                return this.handleErrorResponse(error);
            }));
    }

    // Put Method
    putApi(url, data): Observable<any> {
        return this.http.put<any>(`${this.baseUrlService.getBaseUrl()}` + url, data)
            .pipe(map((response) => {
                return response;
            }), catchError((error: HttpErrorResponse) => {
                return this.handleErrorResponse(error);
            }));
    }

    // Delete Method
    deleteApi(url, id): Observable<any> {
        return this.http.delete<any>(`${this.baseUrlService.getBaseUrl()}` + `${url}/${id}`)
            .pipe(map((response) => {
                return response;
            }), catchError((error: HttpErrorResponse) => {
                return this.handleErrorResponse(error);
            }));
    }

    handleErrorResponse(error: HttpErrorResponse) {
        console.log(error);
        const url: string = this.router.routerState.snapshot.url;
        const that = this;
        const lang = localStorage.getItem('language');
        switch (error.status) {
            case 400:
                swal({
                    title: 'Đã Xảy Ra Lỗi',
                    text: error.error.message,
                    icon: 'error',
                });
                return throwError(new Error(error.error.message));
            case 401:
                this.router.navigate(['auth'], {queryParams: {returnUrl: url}});
                swal({
                    title: 'Đã Xảy Ra Lỗi',
                    text: error.error.message,
                    icon: 'error',
                });
                return throwError(new Error(error.error.message));
            case 403:
                swal({
                    title: 'Đã Xảy Ra Lỗi',
                    text: error.error.message,
                    icon: 'error',
                });
                this.router.navigateByUrl('/login');
                return throwError(new Error(error.error.message));
            default:
                swal({
                    title: 'Đã Xảy Ra Lỗi',
                    text: error.error.message,
                    icon: 'error',
                });
                return throwError(new Error(this.DEFAULT_ERROR_TITLE));
        }
    }
}
