import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BaseUrlService {
  private BASE_URL = 'http://192.168.10.37:8005';

  getBaseUrl() {
    return this.BASE_URL;
  }
  constructor() {
  }
}
