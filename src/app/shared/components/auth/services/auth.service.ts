import {Injectable, OnInit} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {BaseUrlService} from '../../../base_url/base-url.service'; // comment
import {ActivatedRoute, Router} from '@angular/router';
import swal from 'sweetalert';
import {SharedApiService} from '@shared/share_service/shared-api.service';
import * as swal_languages from '@shared/locals/swal_local';

@Injectable({
    providedIn: 'root'
})


export class AuthService implements OnInit {
    private token = 'Bearer ';

    constructor(private http: HttpClient,
                private baseUrl: BaseUrlService,
                private sharedApiService: SharedApiService,
                private router: Router,
                private route: ActivatedRoute) {
    }

    ngOnInit() {
        // get return url from route parameters or default to '/'
    }

    getToken() {
        return this.token;
    }

    login(data) {
        const lang = localStorage.getItem('language')
        return this.sharedApiService.postApi(`/api/user/login`, data).subscribe(res => {
            if (res && res.data) {
                swal({
                    title: swal_languages.swal_local[lang].success.title,
                    text: res.message,
                    icon: 'success',
                });
                localStorage.setItem('currentUser', res.data.sso_token);
            }
            let _returnUrl = '';
            if (this.route.snapshot.queryParams['returnUrl']) {
                _returnUrl = this.route.snapshot.queryParams['returnUrl'];
            }
            this.router.navigateByUrl(_returnUrl);
        });
    }

    register(data) {
        return this.sharedApiService.postApi('/api/user/create', data);
    }

    logout(): void {
        // clear token remove user from local storage to log user out
        this.token = null;
        localStorage.removeItem('currentUser');
    }
}

// Authorization': `Bearer ${AuthService.getToken()
