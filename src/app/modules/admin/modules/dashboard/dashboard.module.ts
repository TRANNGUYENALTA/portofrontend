import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';

import {DashboardRoutingModule} from './dashboard-routing.module';
import {DashBoardComponent} from './dash-board/dash-board.component';
import {SidebarMenuComponent} from './dash-board/components/sidebar-menu/sidebar-menu.component';
import {FooterComponent} from './dash-board/components/footer/footer.component';
import {HeaderComponent} from './dash-board/components/header/header.component';
import {BackToTopComponent} from './dash-board/components/back-to-top/back-to-top.component';

@NgModule({
    declarations: [DashBoardComponent, SidebarMenuComponent, FooterComponent, HeaderComponent, BackToTopComponent],
    imports: [
        CommonModule,
        DashboardRoutingModule
    ]
})
export class DashboardModule {
}
