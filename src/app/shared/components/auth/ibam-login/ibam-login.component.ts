import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {Router} from '@angular/router';
import {Subscription} from 'rxjs';
import {AuthService} from '@shared/components/auth/services/auth.service';
import {User} from '@shared/components/auth/ibam-login/user';

declare var $: any

@Component({
    selector: 'app-ibam-login',
    templateUrl: './ibam-login.component.html',
    styleUrls: ['./ibam-login.component.scss']
})
export class IbamLoginComponent implements OnInit, OnDestroy {

    subscription: Subscription = new Subscription();
    @ViewChild('formLogin') vlForm;
    user: User = new User();

    constructor(private auth: AuthService, private router: Router) {
        $('body').addClass('bg-theme1 bg-theme');
    }

    ngOnInit() {
        localStorage.removeItem('currentUser');
    }

    onSignIn() {
        this.auth.login(this.vlForm.value);
        // this.subscription = this.auth.login(this.vlForm.value);
    }

    resetForm() {
        this.vlForm.reset();
    }

    setFormValue() {
        this.vlForm.setValue({email: 'toi.huynh@alta.com.vn', password: 'admin'});
    }

    ngOnDestroy() {
        // this.subscription.unsubscribe();
    }

}
