import {Component, OnInit} from '@angular/core';
import {mobileMenutoggler} from './header';

declare var jQuery: any;

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

    constructor() {
    }

    ngOnInit() {
        mobileMenutoggler(jQuery);
    }

}
