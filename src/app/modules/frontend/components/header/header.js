export const mobileMenutoggler = (e) => {
    var o = {
        initialised: !1, mobile: !1, init: function () {
            this.initialised || (this.initialised = !0, this.checkMobile(), this.stickyHeader(), this.headerSearchToggle(), this.mMenuIcons(), this.mMenuToggle(), this.mobileMenu())
        }, checkMobile: function () {
            /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent) ? this.mobile = !0 : this.mobile = !1
        }, menuInit: function () {
            e(".menu").superfish({
                popUpSelector: "ul, .megamenu",
                hoverClass: "show",
                delay: 0,
                speed: 80,
                speedOut: 80,
                autoArrows: !0
            })
        }, stickyHeader: function () {
            if (e(".sticky-header").length) {
                new Waypoint.Sticky({element: e(".sticky-header")[0], stuckClass: "fixed", offset: -10});
                if (!e(".header-bottom").find(".logo, .cart-dropdown").length) {
                    var o = e(".header-bottom").find(".container");
                    e(".header").find(".logo, .cart-dropdown").clone(!0).prependTo(o)
                }
            }
            e("main").find(".sticky-header").each(function () {
                new Waypoint.Sticky({element: e(this), stuckClass: "fixed-nav"})
            })
        }, headerSearchToggle: function () {
            e(".search-toggle").on("click", function (o) {
                e(".header-search-wrapper").toggleClass("show"), e("body").toggleClass("is-search-active"), o.preventDefault()
            }), e("body").on("click", function (o) {
                e(".header-search-wrapper").hasClass("show") && (e(".header-search-wrapper").removeClass("show"), e("body").removeClass("is-search-active"))
            }), e(".header-search").on("click", function (e) {
                e.stopPropagation()
            })
        }, mMenuToggle: function () {
            e(".mobile-menu-toggler").on("click", function (o) {
                e("body").toggleClass("mmenu-active"), e(this).toggleClass("active"), o.preventDefault()
            }), e(".mobile-menu-overlay, .mobile-menu-close").on("click", function (o) {
                e("body").removeClass("mmenu-active"), e(".menu-toggler").removeClass("active"), o.preventDefault()
            })
        }, mMenuIcons: function () {
            e(".mobile-menu").find("li").each(function () {
                var o = e(this);
                o.find("ul").length && e("<span/>", {class: "mmenu-btn"}).appendTo(o.children("a"))
            })
        }, mobileMenu: function () {
            e(".mmenu-btn").on("click", function (o) {
                var t = e(this).closest("li"), n = t.find("ul").eq(0), i = t.siblings(".open");
                i.find("ul").eq(0).slideUp(300, function () {
                    i.removeClass("open")
                }), t.hasClass("open") ? n.slideUp(300, function () {
                    t.removeClass("open")
                }) : n.slideDown(300, function () {
                    t.addClass("open")
                }), o.stopPropagation(), o.preventDefault()
            })
        }
    };
    o.init()
}

