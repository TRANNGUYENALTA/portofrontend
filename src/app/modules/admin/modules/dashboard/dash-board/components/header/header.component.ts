import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Router} from '@angular/router';
import {toggleMenuAction} from './header';
import {SharedApiService} from '@shared/share_service/shared-api.service';

@Component({
    selector: 'app-header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
    @Output('changedLanguage') changedLanguage: EventEmitter<string> = new EventEmitter();
    public ln = 'vi';

    constructor(
                private router: Router,
                private shareService: SharedApiService) {
    }

    logout() {
        localStorage.removeItem('currentUser');
        this.router.navigateByUrl('/login');
    }

    resetLanguageAction(_ln) {
        this.changedLanguage.emit(_ln);
        this.ln = _ln;
        this.shareService.ln = _ln;
        localStorage.setItem('language', _ln);
    }

    ngOnInit() {
        const currentLanguage = localStorage.getItem('language');
        if (currentLanguage) {
            this.ln = currentLanguage;
        }
    }

    toggleMenu() {
        toggleMenuAction();
    }

}
